export PATH="$PATH:$HOME/bin"
export PAGER=less
export EDITOR=vi
export MAIL=$HOME/Maildir

autoload zsh/terminfo

# Source any file in .zsh.d
for f in $HOME/.zsh.d/* ; do . $f; done

precmd ()
{
	# set prompt defined in .zsh.d/prompt
	setprompt
}

preexec ()
{
	printf '\033]2;%s\007' "${(V)1}"
}

# CLICOLOR makes 'ls' colorize its output
export CLICOLOR=1

#key binding
bindkey -e      # use emacs keymap
# Customize a bit the bindings
bindkey "$terminfo[kdch1]"    'delete-char'                     #Suppr
bindkey "$terminfo[khome]"    'beginning-of-line'               #Home key
bindkey "$terminfo[kend]"     'end-of-line'                     #End key
bindkey "$terminfo[kcuu1]" history-beginning-search-backward    #Up key
bindkey "$terminfo[kcud1]" history-beginning-search-forward     #Down key

#alias
alias ll="ls -la"

#Make 'cd' being in fact a 'pushd'
DIRSTACKSIZE=8
setopt autopushd pushdminus pushdsilent pushdtohome
alias dh='dirs -v'

#history
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.history

#completion
autoload -U compinit
compinit

#expansion
setopt extended_glob

